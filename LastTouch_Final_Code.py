# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 16:01:19 2020

@author: apathak
"""

# =============================================================================
# Packages
# =============================================================================

import pandas as pd
import datetime as dt
from sqlalchemy import create_engine
import DBConnections as DBcon
import glob
from dateutil import tz

# =============================================================================
#  Paths and creating sql engine
# =============================================================================

homedir = r'C:\Users\apathak\Documents\2020 Projects\MA Facts\Code\Python'
engine = create_engine('sqlite://', echo=False)

# =============================================================================
# Functions
# =============================================================================

def datapull(dbisntance, filepath):
    """
    Provide DB instance and fillpath 
    to read sql script from the
    .sql files and run 
    """
    
    sql_file = open(filepath,"r") 
    query = sql_file.read()
    sql_file.close()
    df = dbisntance.query(query)
    return df,query

def getCampaign(servername,dbname,df,columnkey):
    phonelist = list(set(df[columnkey].to_list()))
    n = 1000
    campaigndf = pd.DataFrame()
    lst = [phonelist[i * n:(i + 1) * n] for i in range((len(phonelist) + n - 1) // n )]
    for items in lst:
        nums = tuple(items)        
        sqlinstSandbox = DBcon.MsqlDB(servername,dbname)    
        cmpg_query = '''SELECT [SalesChannel]
              ,[cmpkey]
              ,[cmpkey_original]
              ,[Channel]
              ,[SubChannel]
              ,[Campaign]
              ,[SubCampaign]
              ,[EndDate]
          FROM [MA_Sandbox].[MAP].[V_Campaign_Master] 
          where cmpkey_original in {} '''.format(nums)
        
        df = sqlinstSandbox.query(cmpg_query)    
        campaigndf= campaigndf.append(df)
        campaigndf = campaigndf.reset_index().drop('index',axis = 1)
    
    campaigndf.columns = campaigndf.columns.str.lower()
    return campaigndf

def getClickpointlead(servername,dbname,df,columnkey):
    unmatched_ids = list(set(df[columnkey].to_list()))
    n = 1000
    unmatched = pd.DataFrame()
    lst = [unmatched_ids[i * n:(i + 1) * n] for i in range((len(unmatched_ids) + n - 1) // n )]
    for items in lst:
        nums = tuple(items)        
        sqlinstSandbox = DBcon.MsqlDB(MKTserver,dbnameSB)    
        match_query = '''select saNumber as contract_id,EmailAddress ,phone1 as leadexec_phone, Campaign  as LeadCampaign ,
        MarketingPartner,DigitsDialed , CallKey , ANI
        FROM ma_clickpoint.[YEARONE].[V_LeadExec]
        where LeadFollowup = 'ConfirmedSale'
        and saNumber in {} '''.format(nums)
        df = sqlinstSandbox.query(match_query)   
        unmatched= unmatched.append(df)
        unmatched = unmatched.reset_index().drop('index',axis = 1)
    
    
    unmatched.columns = unmatched.columns.str.lower()
    unmatched['chnl'] = unmatched.apply(lambda x: x.leadcampaign.lower().split(' ')[0],axis = 1)
    unmatched['data_flag'] = unmatched['chnl'].apply(lambda x: 'inbound' if x == 'inbound' else 'ecomm' if x == 'ecomm' else 'unmatched')
    unmatched['routing'] = unmatched['digitsdialed'].str[:3]
    unmatched['routing_flag'] = (unmatched['routing'] == '107').astype(int)
    unmatched['cmpkey'] = unmatched.apply(lambda x: x['ani'] if x['routing_flag'] == 1 else x['digitsdialed'], axis = 1)
    unmatched['cmpkey'] = unmatched['cmpkey'].fillna('0000000000')
    campaigninfo = unmatched.merge(getCampaign(MKTserver,dbnameSB,unmatched[['cmpkey']],'cmpkey'),how = 'left',left_on = 'cmpkey', right_on = 'cmpkey_original')
    return campaigninfo


def getFlag(df,columns):
    for key in df.data_flag:
        if 'bpo' in df.data_flag and df.BPO_flag == 1 and df.written_date > dt.datetime(2019,8,29,0,0):
            for elements in df[columns]:
                if 'liveops' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = 'inbound'
                    
                    return (salesorc, channel)                   
                    
                elif 'bpo' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'bpo'
                    
                    return (salesorc, channel)
       
        elif 'liveops' in df.data_flag or 'bpo' in df.data_flag:
            for elements in df[columns]:
                if 'liveops' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = 'inbound'
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'inbound'
                        
                    return (salesorc, channel)  
                
        elif 'bpo' in df.data_flag and  df.written_date > dt.datetime(2019,8,29,0,0):
            for elements in df[columns]:
                if 'liveops' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = 'inbound'
                    return (salesorc, channel)
                
                elif 'bpo' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = 'bpo'
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'bpo'
                        
                    return (salesorc, channel)
        
        
                
        elif 'ecomm' in df.data_flag:
            for elements in df[columns]:
                if 'ecomm' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'ecomm'
                        
                    return (salesorc, channel)
        
   
                
        elif 'lead' in df.data_flag:
            for elements in df[columns]:
                                        
                 if 'lead' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'lead'
                        
                    return (salesorc, channel)

        
        elif 'outbound' in df.data_flag or 'outbound-rmi' in df.data_flag:
            for elements in df[columns]:
                                        
                 if 'outbound' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'outbound'
                        
                    return (salesorc, channel)
                
        elif 'outbound-ecomm-abandoned' in df.data_flag:
            for elements in df[columns]:
                                        
                 if 'outbound-ecomm-abandoned' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'outbound-ecomm-abandoned'
                        
                    return (salesorc, channel)        
        
              
        elif 'inbound' in df.data_flag:
            for elements in df[columns]:
                if 'inbound' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'inbound'
                    
                    return (salesorc, channel)
        
        
        
                
        elif 'phone' in df.data_flag:
            for elements in df[columns]:
                if 'phone' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = 'inbound'
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'inbound'
                    
                    return (salesorc, channel)
        
                        
        elif 'unknown' in df.data_flag and  df.written_date > dt.datetime(2019,8,29,0,0):
            for elements in df[columns]:
                if 'unknown' in elements.split('!**!'):
                    channel = elements.split('!**!')[0]
                    salesorc = elements.split('!**!')[1]
                    
                    if channel == None:
                        channel = 'unknown'
                    
                    if salesorc == None :
                        salesorc = 'unknown'
                    
                    return (salesorc, channel)
                
# =============================================================================
# Written Sales and BPO Flag based on first name and last name
# =============================================================================
now = dt.datetime.now()
prodinstance = DBcon.ProdDB('apathak')
print(dt.datetime.now())
wrtnsalesdf,prodquery= datapull(prodinstance ,"C:\\Users\\apathak\\Documents\\2020 projects\\MA Facts\\Code\\SQL\\Written_sales_v1.sql")
print('data pull time for writtensales is ', (dt.datetime.now() - now))
wrtnsalesdf.columns = wrtnsalesdf.columns.str.lower()
wrtnsalesdf=wrtnsalesdf[['written_date', 'contract_id','company_code', 'sales_channel', 'contract_status','last_name', 'first_name', 'name', 'product_description','phone_number', 'internet_address_id','zip_code','state']]


wrtnsalesdf['BPO_flag'] = ((wrtnsalesdf['name'].str.contains('ATG LIVEOPS') | wrtnsalesdf['name'].str.contains('RMI') | wrtnsalesdf['name'].str.contains('IQOR') | wrtnsalesdf['last_name'].str.contains('IQOR') | wrtnsalesdf['last_name'].str.contains( 'RMI') |wrtnsalesdf['last_name'].str.contains('LIVEOPS')) & (wrtnsalesdf.written_date > dt.datetime(2019,8,29,0,0))).astype(int)
print('writtensales is ready')

# =============================================================================
# Sales detail table from MKT sql
# =============================================================================

MKTserver = 'AHS01MARKET01P\AHS_MASVCS_PRD'

print(dt.datetime.now())

dbnameSB = 'AHS_Sandbox'
sqlinstSandbox = DBcon.MsqlDB(MKTserver,dbnameSB)

# eComm
ecom_query ="""SELECT contract_id, entrydate_eff_date,'000000001' as ani,CONCAT(a.[source],'|',a.medium,'|',a.campaign) as originalkey,cmpkey, channel, subchannel, b.campaign,'dummy' as marketingpartner,
       subcampaign,'ecomm' as data_flag
  FROM [AHS_Sandbox].[dbo].[vwPBI_AHS_Sales_eCom_detail] a
    left join [MA_Sandbox].[MAP].[V_Campaign_Master]  b
                on CONCAT(a.[source],'|',a.medium,'|',a.campaign)= b.cmpkey
    where cast(Entrydate_eff_date as date) >= '2018-08-31'
"""

ecomdf = sqlinstSandbox.query(ecom_query)
ecomdf.columns = ecomdf.columns.str.lower()

# leads

leadform_query = """SELECT a.marketingpartner, contract_id, leaduid, a.dateadded, a.brand,'000000001' as ani, effectivedate,entrydate_eff_date,a.cmpkey as originalkey,b.cmpkey, channel, subchannel, b.campaign, subcampaign,phone1,'lead' as data_flag
  FROM AHS_Clickpoint_Data.dbo.ClickPoint_LeadForms c
        left join [AHS_Sandbox].[dbo].[vwPBI_AHS_Sales_LeadForms_detail] a
        on c.UID = a.leadUID 
    left join [MA_Sandbox].[MAP].[V_Campaign_Master]  b
                on a.[cmpkey] = b.[cmpkey]
            where cast(Entrydate_eff_date as date) >= '2018-08-31'
"""

sqlinstSandbox = DBcon.MsqlDB(MKTserver,dbnameSB)    
leadformdf = sqlinstSandbox.query(leadform_query)
leadformdf.columns = leadformdf.columns.str.lower()
outboundformsdf = leadformdf[leadformdf.marketingpartner == 'DirectAgents']
leadformdf = leadformdf[~(leadformdf.marketingpartner == 'DirectAgents')]
outboundformsdf['lead_flag'] = 'dont know'


# inbound
phone_query = """SELECT contract_id, ani, digitsdialed, b.calltype,
       entrydate_eff_date, effdate_eff_date,a.cmpkey as originalkey, b.cmpkey, channel, subchannel, campaign,b.cmpkey_original, subcampaign,'phone' as data_flag,'dummy' as marketingpartner
  FROM [AHS_Sandbox].[dbo].[vwPBI_AHS_Sales_Phone_detail] a
    left join [MA_Sandbox].[MAP].[V_Campaign_Master]  b
                on a.[cmpkey] = b.[cmpkey_original]
                where cast(Entrydate_eff_date as date) >= '2018-08-31'
"""

sqlinstSandbox = DBcon.MsqlDB(MKTserver,dbnameSB)    
phonedf = sqlinstSandbox.query(phone_query)    
phonedf.columns = phonedf.columns.str.lower()
phonedf['cmpkey'] = phonedf['cmpkey'].fillna('0000000000_199001')
phonedf['cmpkey_start_date'] = phonedf.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
phonedf['cmpkey_start_date'] = phonedf.apply(lambda x: dt.date(int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)
phonedf.contract_id = phonedf.contract_id.astype(float)
phonedf['cmpkey_end_date'] = phonedf.sort_values(by=['cmpkey_start_date'], ascending=False).groupby('contract_id')['cmpkey_start_date'].shift(1)
phonedf['cmpkey_end_date'] = phonedf['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
phonedf = phonedf[(phonedf.entrydate_eff_date >= phonedf.cmpkey_start_date) & (phonedf.entrydate_eff_date <= phonedf.cmpkey_end_date)]

# =============================================================================
# Phone calls from BPO 
# =============================================================================

aceyserver = 'mem2adb01p.ahs.com'
dbnameAC = 'aceyus_edw'
sqlinstaceyserver = DBcon.MsqlDB(aceyserver,dbnameAC)   
aceyusdf = sqlinstaceyserver.query("""SELECT RCD.DateTime,RCD.DialedNumberString, CT.EnterpriseName, CT.Description, datepart(yy,RCD.DateTime) as Yr, datepart(mm,RCD.DateTime) as Mo, datepart(dd,RCD.DateTime) as DD  ,ANI ,'dummy' as marketingpartner
  FROM [aceyus_edw].[dbo].[t_Route_Call_Detail] RCD
  LEFT JOIN t_Dialed_Number DN on DN.DialedNumberID = RCD.DialedNumberID
  LEFT JOIN t_Route R on R.RouteID = RCD.RouteID
  LEFT JOIN t_Call_Type CT on CT.CallTypeID = RCD.CallTypeID
  LEFT JOIN t_Script S on S.ScriptID = RCD.ScriptID 
  where RCD.DateTime >= '2019-08-29' and RCD.DateTime <= getdate()
  and CT.EnterpriseName in 
 (
'BPO_RMI_AHS_Cons_Sales',
'BPO_RMI_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales',
'BPO_LiveOps_AHS_Cons_Sales',
'BPO_LiveOps_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales_ValProp',
'BPO_iQor_AHS_Cons_Sales_Other',
'BPO_RMI_AHS_Cons_Sales_Other',
'BPO_LiveOps_AHS_Cons_Other'
)""")

# getting latest call
test = aceyusdf[['ANI','DateTime']].groupby('ANI', as_index=False).max()
aceyusdf = aceyusdf.merge(test,how = 'inner',on = ['ANI','DateTime'])

aceyusdf = aceyusdf.merge(getCampaign(MKTserver,dbnameSB,aceyusdf,'DialedNumberString'),how = 'left',left_on = 'DialedNumberString',right_on = 'cmpkey_original')
aceyusdf.cmpkey = aceyusdf.cmpkey.fillna('0000000000_190001')
aceyusdf['cmpkey_start_date'] = aceyusdf.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
aceyusdf['cmpkey_start_date'] = aceyusdf.apply(lambda x: dt.date(
        int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)

aceyusdf['cmpkey_end_date'] = aceyusdf.sort_values(by=['cmpkey_start_date'], ascending=False).groupby(['DateTime','ANI'])['cmpkey_start_date'].shift(1)

aceyusdf['cmpkey_end_date'] = aceyusdf['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
aceyusdf = aceyusdf[(aceyusdf.DateTime >= aceyusdf.cmpkey_start_date) & (aceyusdf.DateTime <= aceyusdf.cmpkey_end_date)]

aceyusdf['BPO_call'] = (aceyusdf.DateTime >= dt.datetime(2019,8,29,0,0)).astype(int)

phonedf.columns = phonedf.columns.str.lower()
aceyusdf.columns = aceyusdf.columns.str.lower()
phonedf = phonedf.merge(aceyusdf,how = 'left',left_on ='ani', right_on = 'dialednumberstring',suffixes = ('','_BPOaceyus'))
phonedf = phonedf.drop_duplicates().reset_index(drop=True)

phonedf['cmpkey_final'] = phonedf.apply(lambda x: x['cmpkey_BPOaceyus'] if x['bpo_call'] == 1 else x['cmpkey'], axis = 1 )
phonedf['originalkey'] = phonedf.apply(lambda x: x['cmpkey_final'] if x['bpo_call'] == 1 else x['originalkey'], axis = 1 )
phonedf['campaign_final'] = phonedf.apply(lambda x: x['campaign_BPOaceyus'] if x['bpo_call'] == 1 else x['campaign'], axis = 1 )
phonedf['subcampaign_final'] = phonedf.apply(lambda x: x['subcampaign_BPOaceyus'] if x['bpo_call'] == 1 else x['subcampaign'], axis = 1 )
phonedf['channel_final'] = phonedf.apply(lambda x: x['channel_BPOaceyus'] if x['bpo_call'] == 1 else x['channel'], axis = 1 )
phonedf['subchannel_final'] = phonedf.apply(lambda x: x['subchannel_BPOaceyus'] if x['bpo_call'] == 1 else x['subchannel'], axis = 1 )
print(dt.datetime.now())

phonedf = phonedf[['contract_id', 'ani', 'digitsdialed', 'calltype', 'entrydate_eff_date','dialednumberstring','cmpkey_final',
       'effdate_eff_date','bpo_call','originalkey', 'campaign_final', 'subcampaign_final',
       'channel_final', 'subchannel_final','marketingpartner']]
phonedf['data_flag'] = phonedf.apply(lambda x: 'bpo' if x['bpo_call']==1 else 'phone', axis = 1)
phonedf.columns = ['contract_id', 'ani', 'digitsdialed', 'calltype', 'entrydate_eff_date','dialednumberstring','cmpkey',
       'effdate_eff_date','bpo_call', 'originalkey','campaign', 'subcampaign',
       'channel', 'subchannel','marketingpartner','data_flag']
phonedf['marketingpartner'] = 'dummy'
#x = wrtnsalesdf.merge(aceyusdf,how='inner',left_on = 'phone_number',right_on = 'ani')

bpo_cmp = aceyusdf[aceyusdf['datetime'] >= dt.datetime(2019,8,29,0,0)][['dialednumberstring','ani','datetime']].merge(getCampaign(MKTserver,dbnameSB,aceyusdf[['dialednumberstring','ani']],'dialednumberstring'),how = 'inner', left_on = 'dialednumberstring',right_on = 'cmpkey_original')

bpo_cmp['cmpkey_start_date'] = bpo_cmp.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
bpo_cmp['cmpkey_start_date'] = bpo_cmp.apply(lambda x: dt.date(
        int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)
bpo_cmp['cmpkey_end_date'] = bpo_cmp.sort_values(by=['cmpkey_start_date'], ascending=False).groupby(['ani','datetime'])['cmpkey_start_date'].shift(1)
bpo_cmp['cmpkey_end_date'] = bpo_cmp['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
bpo_cmp = bpo_cmp[(bpo_cmp['datetime'] >= bpo_cmp.cmpkey_start_date) & (bpo_cmp['datetime'] <= bpo_cmp.cmpkey_end_date)]

bpo_cmp['originalkey'] = bpo_cmp['cmpkey_original']
bpo_cmp['marketingpartner'] = 'dummy'
#del x
# =============================================================================
# Direct Mail Calls
# =============================================================================
aceyserver = 'mem2adb01p.ahs.com'
dbnameAC = 'aceyus_edw'
sqlinstaceyserver = DBcon.MsqlDB(aceyserver,dbnameAC)   
directmailcalls = sqlinstaceyserver.query('''
SELECT RCD.DateTime,RCD.DialedNumberString, CT.EnterpriseName, CT.Description, datepart(yy,RCD.DateTime) as Yr, datepart(mm,RCD.DateTime) as Mo, datepart(dd,RCD.DateTime) as DD  ,ANI ,'dummy' as marketingpartner
  FROM [aceyus_edw].[dbo].[t_Route_Call_Detail] RCD
  LEFT JOIN t_Dialed_Number DN on DN.DialedNumberID = RCD.DialedNumberID
  LEFT JOIN t_Route R on R.RouteID = RCD.RouteID
  LEFT JOIN t_Call_Type CT on CT.CallTypeID = RCD.CallTypeID
  LEFT JOIN t_Script S on S.ScriptID = RCD.ScriptID 
  where RCD.DateTime >= '2020-04-03'
	and RCD.Label in ('9918333484235', '9918333527645')''')
directmailcalls.columns = directmailcalls.columns.str.lower()
directmailcalls_cmp = directmailcalls[['dialednumberstring','ani','datetime']].merge(getCampaign(MKTserver,dbnameSB,directmailcalls[['dialednumberstring','ani']],'dialednumberstring'),how = 'inner', left_on = 'dialednumberstring',right_on = 'cmpkey_original')

directmailcalls_cmp['cmpkey_start_date'] = directmailcalls_cmp.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
directmailcalls_cmp['cmpkey_start_date'] = directmailcalls_cmp.apply(lambda x: dt.date(
        int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)
directmailcalls_cmp['cmpkey_end_date'] = directmailcalls_cmp.sort_values(by=['cmpkey_start_date'], ascending=False).groupby(['ani','datetime'])['cmpkey_start_date'].shift(1)
directmailcalls_cmp['cmpkey_end_date'] = directmailcalls_cmp['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
directmailcalls_cmp = directmailcalls_cmp[(directmailcalls_cmp['datetime'] >= directmailcalls_cmp.cmpkey_start_date) & (directmailcalls_cmp['datetime'] <= directmailcalls_cmp.cmpkey_end_date)]
directmailcalls_cmp['marketingpartner'] = 'dummy'
directmailcalls_cmp['originalkey'] = directmailcalls_cmp['cmpkey_original']

# =============================================================================
# Other calls in Aceyusdf
# =============================================================================
aceyserver = 'mem2adb01p.ahs.com'
dbnameAC = 'aceyus_edw'
sqlinstaceyserver = DBcon.MsqlDB(aceyserver,dbnameAC)   
spilledcalls = sqlinstaceyserver.query('''
SELECT RCD.DateTime,RCD.DialedNumberString, CT.EnterpriseName, CT.Description, datepart(yy,RCD.DateTime) as Yr, datepart(mm,RCD.DateTime) as Mo, datepart(dd,RCD.DateTime) as DD  ,ANI ,'dummy' as marketingpartner
  FROM [aceyus_edw].[dbo].[t_Route_Call_Detail] RCD
  LEFT JOIN t_Dialed_Number DN on DN.DialedNumberID = RCD.DialedNumberID
  LEFT JOIN t_Route R on R.RouteID = RCD.RouteID
  LEFT JOIN t_Call_Type CT on CT.CallTypeID = RCD.CallTypeID
  LEFT JOIN t_Script S on S.ScriptID = RCD.ScriptID 
   where RCD.DateTime >= '2019-01-01' and RCD.DateTime <= getdate()
  and CT.EnterpriseName in 
 (
'AHS_Cons_Q_Sales',
'AHS_Cons_Q_Porch_DTC',
'AHS_Cons_Q_Sales_RadioDirect',
'AHS_Cons_Q_Sales_Renew',
'AHS_Cons_Q_SalesSpanish',
'AHS_Cons_Q_RepairSearch',
'HSA_Cons_Q_Sales_Renew',
'HSA_Cons_Q_Sales',
'AHS_Cons_Q_Safeco',
'BPO_RMI_AHS_Cons_Sales',
'BPO_RMI_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales',
'BPO_LiveOps_AHS_Cons_Sales',
'BPO_LiveOps_AHS_Cons_Sales_Renew',
'BPO_iQor_AHS_Cons_Sales_ValProp',
'AHS_Cons_Q_Sales_Other',
'BPO_iQor_AHS_Cons_Sales_Other',
'BPO_RMI_AHS_Cons_Sales_Other',
'BPO_LiveOps_AHS_Cons_Other',
'AHS_Cons_Q_Sales_ValProp'

)''')
spilledcalls.columns = spilledcalls.columns.str.lower()
spilledcalls_cmp = spilledcalls[['dialednumberstring','ani','datetime']].merge(getCampaign(MKTserver,dbnameSB,spilledcalls[['dialednumberstring','ani']],'dialednumberstring'),how = 'inner', left_on = 'dialednumberstring',right_on = 'cmpkey_original')

spilledcalls_cmp['cmpkey_start_date'] = spilledcalls_cmp.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
spilledcalls_cmp['cmpkey_start_date'] = spilledcalls_cmp.apply(lambda x: dt.date(
        int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)
spilledcalls_cmp['cmpkey_end_date'] = spilledcalls_cmp.sort_values(by=['cmpkey_start_date'], ascending=False).groupby(['ani','datetime'])['cmpkey_start_date'].shift(1)
spilledcalls_cmp['cmpkey_end_date'] = spilledcalls_cmp['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
spilledcalls_cmp = spilledcalls_cmp[(spilledcalls_cmp['datetime'] >= spilledcalls_cmp.cmpkey_start_date) & (spilledcalls_cmp['datetime'] <= spilledcalls_cmp.cmpkey_end_date)]
spilledcalls_cmp['marketingpartner'] = 'dummy'
spilledcalls_cmp['originalkey'] = directmailcalls_cmp['cmpkey_original']

# =============================================================================
# Amazon Connect Sales matching
# =============================================================================


prodinstance = DBcon.SnowFlakeDB('apathak','ELT_DEV','ELT_DEV_RL')
print(dt.datetime.now())

amazonconnect = prodinstance.query('''
select 

"CustomerEndpoint.Address"
,"SystemEndpoint.Address"
,"Queue.Name"
,"Agent.AfterContactWorkEndTimestamp"
,"InitiationMethod"
,"Agent.ConnectedToAgentTimestamp"
,"Agent.RoutingProfile.Name"

from "PARTY"."LANDING_S3"."VW_SALES_CTR_PROD_JSON"
where "Agent.ConnectedToAgentTimestamp" is not null 
and "Queue.Name" LIKE '%Consumer Sales%'
AND "InitiationMethod" IN ('TRANSFER','INBOUND')
                                 ''')

amazonconnect['digitsdialed'] = amazonconnect['SystemEndpoint.Address'].str.slice(2)
amazonconnect['phone_number'] = amazonconnect['CustomerEndpoint.Address'].str.slice(2)

# Timezone conversion
from_zone = tz.tzutc()
to_zone = tz.tzlocal()

amazonconnect['CallTime'] = pd.to_datetime(amazonconnect['Agent.ConnectedToAgentTimestamp']).apply(lambda x: x.replace(tzinfo=from_zone).astimezone(to_zone)) - dt.timedelta(hours = 7)

test = wrtnsalesdf.merge(amazonconnect,how = 'inner', on = 'phone_number')
test['datetime_last'] =  (test['CallTime'] + dt.timedelta(days = 1))
test['datetime_win']= test['datetime_last'].apply(lambda a: dt.datetime(a.year,a.month,a.day,0,0,0))

test = test[(pd.to_datetime(test.written_date) >= test['CallTime'].dt.tz_localize(None)) & (pd.to_datetime(test.written_date) < test['datetime_win'])].drop_duplicates()

test = test.merge(getCampaign(MKTserver,dbnameSB,test,'digitsdialed'),left_on = 'digitsdialed', right_on = 'cmpkey_original',how = 'left')

test.cmpkey = test.cmpkey.fillna('0000000000_190001')
test['cmpkey_start_date'] = test.apply(lambda x: x.cmpkey.split('_')[1] if len(x.cmpkey.split('_')) == 2 else '190001',axis = 1) + '01'

test['cmpkey_start_date'] = test.apply(lambda x: dt.date(
        int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)

test['cmpkey_end_date'] = test.sort_values(by=['cmpkey_start_date'], ascending=False).groupby('contract_id')['cmpkey_start_date'].shift(1)

test['cmpkey_end_date'] = test['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))

test['cmpkey_end_date'] = test['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
test = test[(test.written_date >= test.cmpkey_start_date) & (test.written_date <= test.cmpkey_end_date)]

test.columns = ['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'phone_number', 'internet_address_id',
       'zip_code', 'state', 'BPO_flag', 'CustomerEndpoint.Address',
       'SystemEndpoint.Address', 'Queue.Name',
       'Agent.AfterContactWorkEndTimestamp', 'data_flag',
       'Agent.ConnectedToAgentTimestamp', 'Agent.RoutingProfile.Name',
       'digitsdialed', 'CallTime', 'datetime_last', 'datetime_win',
       'saleschannel', 'cmpkey', 'cmpkey_original', 'channel', 'subchannel',
       'campaign', 'subcampaign', 'enddate', 'cmpkey_start_date',
       'cmpkey_end_date']

test = test.merge(test.groupby('contract_id').agg({'CallTime':'min'}).reset_index(),how = 'inner', on = ['contract_id','CallTime'])

connectdf = test.drop_duplicates().reset_index(drop = True)
connectdf['data_flag'] = connectdf['data_flag'].apply(lambda x: 'inbound' if x == 'TRANSFER' else x.lower())

connectdf['BPO_flag'] = ((connectdf['Agent.RoutingProfile.Name'].str.contains('RMI') | connectdf['Agent.RoutingProfile.Name'].str.contains('IQOR'))).astype(int)
connectdf['originalkey'] = connectdf['cmpkey']
connectdf['data_flag'] = connectdf.apply(lambda x: 'inbound' if x['BPO_flag'] != 1 else 'bpo',axis = 1)
connectdf['marketingpartner'] = 'dummy'

# =============================================================================
#  Livops
# =============================================================================
print("let's check")
path = r'Y:\Marketing\liveops_sftp' # use your path
all_files = glob.glob(path + "/*.xls")
li = []

filenames = pd.DataFrame(all_files)
filenames.columns = ['filenames']
livpath = r'C:\Users\apathak\Documents\2020 Projects\MA Facts\Data\Interim Output'
liveopsbasedf = pd.read_csv(livpath + '/AHS_04_23_20.csv',parse_dates = True)
liveopsbasedf.columns = liveopsbasedf.columns.str.replace(' ','').str.lower()
liveopsbasedf = liveopsbasedf[~liveopsbasedf.date.isna()]
liveopsbasedf['time'] = liveopsbasedf.date.str.replace('-','').str.lstrip().str.rstrip().apply(lambda y: y.split(' ')[0])
for dts in range(20200101,liveopsbasedf['time'].astype(int).max()):
    filenames['flag'] = filenames.filenames.str.contains(str(dts))
    filenames = filenames[filenames.flag == 0]

for filename in filenames.filenames:
    df = pd.read_csv(filename, sep='\t')
    li.append(df)
liveopsbasedf = liveopsbasedf[liveopsbasedf['time']!= str(liveopsbasedf['time'].astype(int).max())]


liveops = pd.concat(li, axis=0, ignore_index=True)
liveops.columns = liveops.columns.str.replace(' ','').str.lower()
liveops = liveops[['date', 'programname', 'campaignname', 'inbound/outbound',
       'source', 'calltype', 'callerani', 'timezone', 'phonenumber',
       'dispositiontype', 'disposition', 'contractnumber', 'contractnumber2',
       'contractnumber3', 'contractnumber4', 'contractnumber5']]

liveops = liveops.append(liveopsbasedf).reset_index(drop = True)
liveops.to_csv(livpath + '/AHS_04_23_20.csv',index = False)
liveops['campaignname'] = liveops['campaignname'].fillna('unknown')
liveops = liveops[liveops['campaignname'].str.contains('DRTV')]
liveops['phone'] = liveops['phonenumber'].str.replace('-','')
liveops.phone = liveops.phone.str[1:]
liveops.phone = liveops.phone.fillna('0000000000')


liveops['channel'] = 'Broadcast'
liveops['subchannel'] = 'TV-DR'
liveops['campaign'] = 'Liveops (DRTV)'
liveops['marketingpartner'] = 'dummy'


liveops = liveops.melt(id_vars=['date', 'programname', 'campaignname', 'inbound/outbound', 'source',
       'calltype', 'callerani', 'timezone', 'phonenumber', 'dispositiontype',
       'disposition','phone', 'channel', 'subchannel',
       'campaign', 'marketingpartner'], 
        var_name="contractnumbers", 
        value_name="contract_id")
liveops.contract_id = liveops.contract_id.fillna('1231231231').str.replace('o','').str.replace(' ','').fillna('111')
liveops.contract_id = liveops.apply(lambda y: float(y.contract_id) if y.contract_id != '' else 1231323132,axis = 1)


print("nope all good")


# =============================================================================
#   MatchBack Process Start
# =============================================================================

# column standardization
ecomdf.contract_id = ecomdf.contract_id.astype(float)
phonedf.contract_id = phonedf.contract_id.astype(float)
leadformdf.contract_id = leadformdf.contract_id.astype(float)
outboundformsdf.contract_id = outboundformsdf.contract_id.astype(float)
connectdf.contract_id = connectdf.contract_id.astype(float)

bpo_cmp.columns= bpo_cmp.columns.str.lower()
ecomdf.columns= ecomdf.columns.str.lower()
phonedf.columns = phonedf.columns.str.lower()
connectdf.columns = connectdf.columns.str.lower()
leadformdf.columns= leadformdf.columns.str.lower()
outboundformsdf.columns = outboundformsdf.columns.str.lower()
liveops.columns = liveops.columns.str.lower()

main_columns = ['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'phone_number', 'internet_address_id','zip_code','state','originalkey',
       'BPO_flag','channel', 'subchannel', 'campaign', 'subcampaign','data_flag','cmpkey','ani','marketingpartner']




# joins
# ecomm
ecomm = wrtnsalesdf.merge(ecomdf,how = 'inner',on='contract_id',suffixes=('','_ecomm'))[main_columns]
ecomm['data_flag'] = ecomm.apply(lambda x: 'inbound' if ('RMI' in x['name'] )|('LIVEOPS' in x['name'] ) else
     x['data_flag'],axis = 1)
ecomm['campaign'] = ecomm.apply(lambda x: 'Digital' if ('RMI' in x['name'] ) else x.campaign , axis = 1)
ecomm['subcampaign'] = ecomm.apply(lambda x: 'PPC Affiliate' if ('RMI' in x['name'] ) else x.subcampaign , axis = 1)
ecomm['channel'] = ecomm.apply(lambda x: 'RMI' if ('RMI' in x['name'] ) else x.channel , axis = 1)
ecomm['subchannel'] = ecomm.apply(lambda x: 'RMI Funnel' if ('RMI' in x['name'] ) else x.subchannel , axis = 1)

#spilledcalls_cmp
spilled = wrtnsalesdf.merge(spilledcalls_cmp,how = 'inner',left_on = 'phone_number',right_on = 'ani',suffixes=('','_bpo_cmp'))
spilled['datetime_last'] =  (spilled.datetime + dt.timedelta(days = 1))
spilled['datetime_win']= spilled['datetime_last'].apply(lambda a: dt.datetime(a.year,a.month,a.day,0,0,0))
spilled =spilled[(spilled.written_date >= spilled.datetime)  & (spilled.written_date <= spilled.datetime_win)]
spilled['date_rank'] = spilled.groupby(['contract_id'])['datetime'].rank(method = 'max')
spilled = spilled[spilled.date_rank == 1]

spilled['data_flag'] = 'inbound'
spilled = spilled[main_columns]


#bpo
bpo = wrtnsalesdf[wrtnsalesdf.BPO_flag == 1].merge(bpo_cmp,how = 'inner',left_on = 'phone_number',right_on = 'ani',suffixes=('','_bpo_cmp'))
bpo['datetime_last'] =  (bpo.datetime + dt.timedelta(days = 1))
bpo['datetime_win']= bpo['datetime_last'].apply(lambda a: dt.datetime(a.year,a.month,a.day,0,0,0))
bpo =bpo[(bpo.written_date >= bpo.datetime)  & (bpo.written_date <= bpo.datetime_win)]
bpo['date_rank'] = bpo.groupby(['contract_id'])['datetime'].rank(method = 'max')
bpo = bpo[bpo.date_rank == 1]

bpo['data_flag'] = 'bpo'
bpo = bpo[main_columns]

#directmail
dm = wrtnsalesdf.merge(directmailcalls_cmp,how = 'inner',left_on = 'phone_number',right_on = 'ani',suffixes=('','_bpo_cmp'))
dm['datetime_last'] =  (dm.datetime + dt.timedelta(days = 1))
dm['datetime_win']= dm['datetime_last'].apply(lambda a: dt.datetime(a.year,a.month,a.day,0,0,0))
dm =dm[(dm.written_date >= dm.datetime)  & (dm.written_date <= dm.datetime_win)]
dm['date_rank'] = dm.groupby(['contract_id'])['datetime'].rank(method = 'max')
dm = dm[dm.date_rank == 1]

dm['data_flag'] = 'inbound'
dm = dm[main_columns]


# phone
phone = wrtnsalesdf.merge(phonedf,how = 'inner',on='contract_id',suffixes=('','_phone'))[main_columns]

#leads
lead = wrtnsalesdf.merge(leadformdf,how = 'inner',on='contract_id',suffixes=('','_lead'))[main_columns]
outbound = wrtnsalesdf.merge(outboundformsdf,how = 'inner',on='contract_id',suffixes=('','_outbound'))[main_columns]

# join on phone numbers
wrtnsalesdf.phone_number = wrtnsalesdf.phone_number.fillna(1001000100)
wrtnsalesdf.phone_number = wrtnsalesdf.phone_number.apply(lambda x: '1' + str(x))
outboundformsdf.phone1 = outboundformsdf.phone1.apply(lambda x: '1' + str(x))
outboundnumber = wrtnsalesdf.merge(outboundformsdf,how = 'inner',left_on = 'phone_number', right_on = 'phone1',suffixes=('','_outbound_ANI'))[main_columns]
#outboundnumber['data_flag'] = outboundnumber.apply(lambda x: 'outbound' if x.written_date < dt.datetime(2020,1,4) else 'outbound-rmi',axis = 1)

# liveops
#liveops['caller ani'] = liveops['caller ani'].str.replace('-','')
liveops['data_flag'] = 'liveops'
livops= wrtnsalesdf.merge(liveops,how = 'inner', on = 'contract_id',suffixes=('','_livops'))

livops['originalkey'] = livops.phone
livops['cmpkey'] = livops.phone
livops['subcampaign'] = 'unknown'
livops.columns =['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'phone_number', 'internet_address_id',
       'zip_code', 'state', 'BPO_flag', 'date', 'programname', 'campaignname',
       'inbound/outbound', 'source', 'calltype', 'ani', 'timezone',
       'phonenumber', 'dispositiontype', 'disposition', 'phone', 'channel',
       'subchannel', 'campaign', 'marketingpartner', 'contractnumbers',
       'data_flag', 'originalkey', 'cmpkey', 'subcampaign']
livops = livops.drop_duplicates()[main_columns]

# amazonconnect data
connectdf.columns = ['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'phone_number', 'internet_address_id',
       'zip_code', 'state', 'BPO_flag', 'ani',
       'systemendpoint.address', 'queue.name',
       'agent.aftercontactworkendtimestamp', 'data_flag',
       'agent.connectedtoagenttimestamp', 'agent.routingprofile.name',
       'digitsdialed', 'calltime', 'datetime_last', 'datetime_win',
       'saleschannel', 'cmpkey','cmpkey_original', 'channel', 'subchannel',
       'campaign', 'subcampaign', 'enddate', 'cmpkey_start_date',
       'cmpkey_end_date','originalkey','marketingpartner']

connectdf['cmpkey'] = connectdf['cmpkey'].apply(lambda x: x.split('_')[0])
bpo['cmpkey'] = bpo['cmpkey'].apply(lambda x: x.split('_')[0])
spilled['cmpkey'] = spilled['cmpkey'].apply(lambda x: x.split('_')[0])
dm['cmpkey'] = dm['cmpkey'].apply(lambda x: x.split('_')[0])
phone['cmpkey'] = phone['cmpkey'].apply(lambda x: x.split('_')[0])


# append data
data = ecomm.append([bpo,phone,lead,outbound,outboundnumber,livops,connectdf[main_columns],dm,livops,spilled]).drop_duplicates().reset_index(drop=True)

# =============================================================================
# Matching upnmatched
# =============================================================================

unmatched_contract = wrtnsalesdf[(~wrtnsalesdf.contract_id.isin(data.contract_id))]
data_unmatched = getClickpointlead(MKTserver,dbnameSB,unmatched_contract,'contract_id')[['contract_id', 'emailaddress', 'leadexec_phone', 'leadcampaign',
       'marketingpartner', 'digitsdialed', 'callkey', 'ani', 'chnl',
       'data_flag', 'routing', 'routing_flag', 'saleschannel',
       'cmpkey_y', 'cmpkey_original', 'channel', 'subchannel', 'campaign',
       'subcampaign', 'enddate']]

data_unmatched.columns = ['contract_id', 'emailaddress', 'leadexec_phone', 'leadcampaign',
       'marketingpartner', 'digitsdialed', 'callkey', 'ani', 'chnl',
       'data_flag', 'routing', 'routing_flag', 'saleschannel',
       'cmpkey', 'cmpkey_original', 'channel', 'subchannel', 'campaign',
       'subcampaign', 'enddate']
data_unmatched['originalkey'] = data_unmatched['cmpkey']
data_unmatched.cmpkey = data_unmatched.cmpkey.fillna('0000000000_190001')
data_unmatched['cmpkey_start_date'] = data_unmatched.apply(lambda x: x.cmpkey.split('_')[1],axis = 1) + '01'
data_unmatched['cmpkey_start_date'] = data_unmatched.apply(lambda x: dt.date(int(x.cmpkey_start_date[:4]),int(x.cmpkey_start_date[4:6]),int(x.cmpkey_start_date[6:])),axis = 1)
data_unmatched.contract_id = data_unmatched.contract_id.astype(float)
data_unmatched['cmpkey_end_date'] = data_unmatched.sort_values(by=['cmpkey_start_date'], ascending=False).groupby('contract_id')['cmpkey_start_date'].shift(1)


data_unmatched['cmpkey_end_date'] = data_unmatched['cmpkey_end_date'].fillna(dt.date.today() + dt.timedelta(days = 1))
data_unmathd = wrtnsalesdf.merge(data_unmatched,how = 'inner',on = 'contract_id')
data_unmathd = data_unmathd[(data_unmathd.written_date >= data_unmathd.cmpkey_start_date) & (data_unmathd.written_date <= data_unmathd.cmpkey_end_date)][main_columns]

data = data.append(data_unmathd).drop_duplicates().reset_index(drop=True)
unmatched_contract = wrtnsalesdf[(~wrtnsalesdf.contract_id.isin(data.contract_id))]

sqlinstSandbox = DBcon.MsqlDB(MKTserver,dbnameSB)    
  
leads_query = """SELECT  LeadUID ,Phone1,CampaignType,MarketingPartner 
 FROM [AHS_Clickpoint_Data].[dbo].[ClickPoint_SalesForms_AgentDesktop_AHS]
  where (CampaignType =  'eCom Abandoned with Phone' or CampaignType =  'eComm Abandon with Phone') and Status like '%sold%'   and DateAdded > '2019-01-01'"""

leadsdf = sqlinstSandbox.query(leads_query)  
leadsdf.columns = leadsdf.columns.str.lower()
leadsdf['phone_number'] = leadsdf.phone1.apply(lambda x: '1' + str(x))

leads = unmatched_contract.merge(leadsdf,how = 'inner',on='phone_number')
leads['data_flag'] = 'outbound-ecomm-abandoned'
leads['channel'] = 'Digital'
leads['subcampaign'] = 'unknown'
leads['subchannel'] = 'AHS.com'
leads['campaign'] = 'unknown'
leads['cmpkey'] = 'unknown'
leads['originalkey'] = 'unknown'
leads['ani'] = '0000000001'

data = data.append(leads[main_columns]).drop_duplicates().reset_index(drop=True)

unmatched_contract = wrtnsalesdf[(~wrtnsalesdf.contract_id.isin(data.contract_id))]

unmatched_contract = unmatched_contract.reset_index(drop = True)
unmatched_contract = unmatched_contract[['written_date', 'contract_id', 'company_code', 'sales_channel','contract_status', 'last_name', 'first_name', 'name',      'product_description', 'BPO_flag', 'phone_number','internet_address_id','zip_code','state']]
aceyusdf['datetime_last'] =  (aceyusdf.datetime + dt.timedelta(days = 1))
aceyusdf['datetime_win']= aceyusdf['datetime_last'].apply(lambda a: dt.datetime(a.year,a.month,a.day,0,0,0))

csv_database = create_engine('sqlite:///csv_database.db')
unmatched_contract.to_sql('contracts', csv_database, if_exists='replace')
aceyusdf[['datetime','datetime_win', 'dialednumberstring', 'enterprisename', 'description', 'yr',
       'mo', 'dd', 'ani', 'saleschannel', 'cmpkey', 'channel', 'subchannel',
       'campaign', 'subcampaign', 'bpo_call']].to_sql('aceyusdata', csv_database, if_exists='replace')


bpo_matched = pd.read_sql_query('''SELECT * FROM 
               contracts a
               inner join aceyusdata b
               on a.written_date between b.datetime and b.datetime_win
               and BPO_flag = 1
               
               ''',csv_database)
bpo_matched['data_flag'] = 'bpo'
bpo_matched['datetime'] = bpo_matched['datetime'].fillna(dt.datetime(2016, 1, 1))
bpo_matched['datetime'] = pd.to_datetime(bpo_matched['datetime'])
bpo_matched['written_date'] = pd.to_datetime(bpo_matched['written_date'])

bpo_matched['date_rank'] = bpo_matched.groupby(['contract_id'])['datetime'].rank(method = 'max')
bpo_matched['originalkey'] = bpo_matched.cmpkey
bpo_matched['marketingpartner'] = 'dummy'

bpo_matched = bpo_matched[bpo_matched.date_rank == 1][main_columns]
bpo_matched['cmpkey'] = bpo_matched['cmpkey'].apply(lambda x: x.split('_')[0])
data = data.append(bpo_matched)

unmatched_contract = wrtnsalesdf[(~wrtnsalesdf.contract_id.isin(data.contract_id))]

unmatched_contract = unmatched_contract[['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'BPO_flag','phone_number','internet_address_id','zip_code','state']]
unmatched_contract['data_flag'] = 'unmatched'
unmatched_contract['channel'] = 'unknown'
unmatched_contract['subchannel'] = 'unknown'
unmatched_contract['campaign'] = 'unknown'
unmatched_contract['subcampaign'] = 'unknown' 
unmatched_contract['ani'] = '0000000001' 
unmatched_contract['marketingpartner'] = 'dummy'

data = data.append(unmatched_contract).drop_duplicates().reset_index(drop = True)

# =============================================================================
# Data Standarization
# =============================================================================

data.phone_number = data.phone_number.fillna('0000000000')
data.ani = data.ani.fillna('0000000000')
data.channel = data.channel.fillna('unknown')
data.originalkey = data.originalkey.fillna('unknown')
data.cmpkey = data.cmpkey.fillna('unknown')
data.subchannel = data.subchannel.fillna('unknown')
data.campaign = data.campaign.fillna('unknown')
data.subcampaign= data.subcampaign.fillna('unknown')
data.marketingpartner= data.marketingpartner.fillna('unknown')
data.data_flag = data.data_flag.fillna('unknown')

data.cmpkey = data.cmpkey + '!**!' + data.data_flag
data.originalkey = data.originalkey + '!**!' + data.data_flag
data.channel = data.channel + '!**!' + data.data_flag
data.subchannel = data.subchannel + '!**!' + data.data_flag
data.campaign = data.campaign + '!**!' + data.data_flag
data.subcampaign= data.subcampaign + '!**!' + data.data_flag
data.marketingpartner= data.marketingpartner + '!**!' + data.data_flag
data.data_flag = data.apply(lambda x: 'bpo' if x.BPO_flag == 1 else x.data_flag, axis = 1)

# =============================================================================
# Rolling up
# =============================================================================


bpo_flag= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state']).agg({'BPO_flag':'max'})
originalkey= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state'])['originalkey'].apply(lambda x: ', '.join(x))
ani= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state'])['ani'].apply(lambda x: ', '.join(x))
mktpartner= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state'])['marketingpartner'].apply(lambda x: ', '.join(x))
data_flag= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state'])['data_flag'].apply(lambda x: ', '.join(x))
cmpkey= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description','zip_code','state'])['cmpkey'].apply(lambda x: ', '.join(x))
phone= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel','contract_status', 'last_name', 'first_name', 'name', 'product_description','zip_code','state'])['phone_number'].apply(lambda x: ', '.join(x))
channel= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel','contract_status', 'last_name', 'first_name', 'name', 'product_description','zip_code','state'])['channel'].apply(lambda x: ', '.join(x))
subchannel= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel','contract_status', 'last_name', 'first_name', 'name', 'product_description','zip_code','state'])['subchannel'].apply(lambda x: ', '.join(x))
campaign= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',  'contract_status', 'last_name', 'first_name', 'name', 'product_description','zip_code','state'])['campaign'].apply(lambda x: ', '.join(x))
subcampaign= data.groupby(['written_date', 'contract_id', 'company_code', 'sales_channel',   'contract_status', 'last_name', 'first_name', 'name', 'product_description','zip_code','state'])['subcampaign'].apply(lambda x: ', '.join(x))

finaldf = pd.concat([bpo_flag,cmpkey,data_flag,phone,channel,subchannel,campaign,subcampaign,ani,originalkey,mktpartner],axis = 1).reset_index()


# =============================================================================
# deduping multiples
# =============================================================================


finaldf['data_flag'] = finaldf.apply(lambda x: set(x['data_flag'].split(', ')),axis = 1)
finaldf['cmpkey'] = finaldf.apply(lambda x: set(x['cmpkey'].split(', ')),axis = 1)
finaldf['originalkey'] = finaldf.apply(lambda x: set(x['originalkey'].split(', ')),axis = 1)
finaldf['ani'] = finaldf.apply(lambda x: set(x['ani'].split(', ')),axis = 1)
finaldf['phone_number'] = finaldf.apply(lambda x: set(x['phone_number'].split(', ')),axis = 1)
finaldf['channel'] = finaldf.apply(lambda x: set(x['channel'].split(', ')),axis = 1)
finaldf['subchannel'] = finaldf.apply(lambda x: set(x['subchannel'].split(', ')),axis = 1)
finaldf['campaign'] = finaldf.apply(lambda x: set(x['campaign'].split(', ')),axis = 1)
finaldf['subcampaign'] = finaldf.apply(lambda x: set(x['subcampaign'].split(', ')),axis = 1)
finaldf['marketingpartner'] = finaldf.apply(lambda x: set(x['marketingpartner'].split(', ')),axis = 1)

finaldf[['name']] = finaldf[['name']].astype(str)

# =============================================================================
#  Creating Flags
# =============================================================================

finaldf['MktOrgcmpkey'] = finaldf.apply(lambda x: getFlag(x,'originalkey'), axis = 1)               
finaldf['Mktpartner'] = finaldf.apply(lambda x: getFlag(x,'marketingpartner'), axis = 1)               
finaldf['Mktcmpkey'] = finaldf.apply(lambda x: getFlag(x,'cmpkey'), axis = 1)
finaldf['MktChannel'] = finaldf.apply(lambda x: getFlag(x,'channel'), axis = 1)
finaldf['MktsubChannel'] = finaldf.apply(lambda x: getFlag(x,'subchannel'), axis = 1)
finaldf['MktCampaign'] = finaldf.apply(lambda x: getFlag(x,'campaign'), axis = 1)
finaldf['MktsubCampaign'] = finaldf.apply(lambda x: getFlag(x,'subcampaign'), axis = 1)
finaldf['obleadflag'] = finaldf.name.str.contains("OB")
finaldf['ecommleadflag'] = finaldf.name.str.contains("ATG")


finaldf['Mktpartner'] = finaldf['Mktpartner'].apply(lambda x: x[1] if x is not None else 'dummy')

finaldf['salesorc'] = finaldf.apply(lambda x: tuple(x.data_flag)[0] if x['MktChannel'] == None else tuple(set([x['MktChannel'][0],x['MktsubChannel'][0],x['MktCampaign'][0],x['MktsubCampaign'][0]]))[0] ,axis = 1)

finaldf['salesorc'] = finaldf.apply(lambda x: x.salesorc if x.salesorc != 'lead' else  
    'ecomm' if (x['ecommleadflag'] == True and x['obleadflag'] == False and x['salesorc'] == 'lead')
    else  
    'outbound',axis = 1)
finaldf['MktChannel'] = finaldf['MktChannel'].fillna('unknown')
finaldf['MktsubChannel'] = finaldf['MktsubChannel'].fillna('unknown')
finaldf['MktCampaign'] = finaldf['MktCampaign'].fillna('unknown')
finaldf['MktsubCampaign'] = finaldf['MktsubCampaign'].fillna('unknown')
finaldf['MktChannel'] = finaldf['MktChannel'].apply(lambda x: x[1] if type(x) == tuple else x)
finaldf['MktsubChannel'] = finaldf['MktsubChannel'].apply(lambda x: x[1] if type(x) == tuple else x)
finaldf['MktCampaign'] = finaldf['MktCampaign'].apply(lambda x: x[1] if type(x) == tuple else x)
finaldf['MktsubCampaign'] = finaldf['MktsubCampaign'].apply(lambda x: x[1] if type(x) == tuple else x)
finaldf['Mktcmpkey'] = finaldf['Mktcmpkey'].apply(lambda x: x[1] if type(x) == tuple else x)

finaldf['MktChanneldigital'] = finaldf.MktsubChannel.str.lower().str.contains('affiliates|email|ahs.com|paid search|ppc affiliate') 
finaldf['MktChannelBroad'] = finaldf.MktsubChannel.str.lower().str.contains('tv-dr') 
finaldf['MktChannelDM'] = finaldf.MktsubChannel.str.lower().str.contains('cancels|formers|leads not sold|prospects') 

finaldf['MktChannel'] = finaldf.apply(lambda x: x['MktChannel'] if (x['salesorc'] != 'bpo' and x['MktChannel'] != 'unknown') or (x['salesorc'] == 'bpo' and x['MktChannel'] != 'unknown')  else 'Digital' if x.MktChanneldigital else 'Broadcast'
        if x.MktChannelBroad else 'Direct Mail' if x.MktChannelDM else  x.MktChannel, axis = 1)

finaldf['salesfunnel'] = finaldf['salesorc'].apply(lambda x: 'inbound' if x == 'bpo' else x)

finaldf['salesfunnel'] = finaldf.apply(lambda x: x.salesfunnel if x.salesfunnel != 'unmatched' else 
       'outbound' if 'OB ' in x['name'] else
       'inbound' if ('SERVSMART ' in x['name'])|('INBOUND ' in x['name']) else
       'ecomm' if 'ATG ' in x['name']  else x.salesfunnel, axis = 1)

finaldf['salesfunnel'] = finaldf.salesfunnel.apply(lambda x: x if x != 'outbound-rmi' else 'outbound')
finaldf['salesfunnel'] = finaldf.apply(lambda x: 'inbound' if x.BPO_flag == 1 else x['salesfunnel'],axis = 1)

finaldf['subsalesfunnel'] = finaldf.apply(lambda x: 
    
    x.salesfunnel + '- RMI' if (('RMI' in x['name'])|('RMI' in x['last_name'])) and x.salesfunnel == 'inbound' 
    
    else 
    
    x.salesfunnel + '- iQor' if (('IQOR' in x['name'])|('IQOR' in x['last_name'])) and x.salesfunnel == 'inbound'
    
    else 
    
    x.salesfunnel + '- LiveOps' if (('LIVEOPS' in x['name'])|('LIVEOPS' in x['last_name'])) and x.salesfunnel == 'inbound'
    
    else 
    
    x.salesfunnel + '- AHS' if x.salesfunnel == 'inbound'
    
    else 
    
    'outbound-rmi' if ('outbound-rmi' in x.data_flag) and x.salesfunnel == 'outbound'
    
    else 
    
    'outbound-ahs' if  x.salesfunnel == 'outbound'
    
    else
    
    x.salesfunnel, axis = 1)
    
finaldf['year'] = finaldf.written_date.dt.year
finaldf['month'] = finaldf.written_date.dt.month
finaldf['day'] = finaldf.written_date.dt.day
finaldf['week'] = finaldf.written_date.dt.weekofyear

col_list = ['salesfunnel','subsalesfunnel','MktChannel','MktsubChannel','MktCampaign','MktsubCampaign']
for col in col_list:
    finaldf[col] = finaldf[col].str.upper()

summary = finaldf.groupby(['year','month','salesfunnel','subsalesfunnel','salesorc','MktChannel','MktsubChannel','MktCampaign','MktsubCampaign']).agg({'contract_id':pd.Series.nunique}).reset_index()

# =============================================================================
# 
# =============================================================================
campaign_file = pd.read_csv('Y:/Marketing/MA Facts - LastTouch MatchBack/CampaignMaster_wNewChannelMaps.csv',encoding = "ISO-8859-1", engine='python')
campaign_file.columns = campaign_file.columns.str.replace(' ','').str.lower()
campaign_file.enddate = campaign_file.enddate.fillna('2099-12-31') 
campaign_file.enddate = pd.to_datetime(campaign_file.enddate)
campaign_file.startdate = campaign_file.startdate.fillna('2099-12-31') 
campaign_file.startdate = pd.to_datetime(campaign_file.startdate)
#
#ecommpath = r'C:\Users\apathak\Documents\2020 Projects\MA Facts\Files'
#ecommgrouping = pd.read_csv(ecommpath + '/Call_ecomm_grouping.csv')
#ecommgrouping['Campaign'] = ecommgrouping['Campaign'].fillna('NULL')
#ecommgrouping['Subcampaign'] = ecommgrouping['Subcampaign'].fillna('')
#ecommgrouping.columns = ecommgrouping.columns.str.lower()
#
#ecommgrouping['channel'] = ecommgrouping['channel'].str.upper()
#ecommgrouping['subchannel'] = ecommgrouping['subchannel'].str.upper()
#ecommgrouping['campaign'] = ecommgrouping['campaign'].str.upper()
#ecommgrouping['subcampaign'] = ecommgrouping['subcampaign'].str.upper()

cols = ['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'zip_code', 'state', 'marketingpartner',
       'BPO_flag', 'cmpkey', 'data_flag', 'phone_number', 'channel',
       'subchannel', 'campaign', 'subcampaign', 'ani', 'originalkey',
       'MktOrgcmpkey', 'Mktcmpkey', 'MktChannel', 'MktsubChannel',
       'MktCampaign', 'MktsubCampaign', 'obleadflag', 'ecommleadflag',
       'salesorc', 'MktChanneldigital', 'MktChannelBroad', 'MktChannelDM',
       'salesfunnel', 'subsalesfunnel', 'year', 'month', 'day', 'week','Mktpartner','channelmapping(marketingperformancereport)',
       'sub-channelmapping(marketingperformancereport)','startdate','enddate']

check = finaldf.merge(campaign_file,how = 'left', left_on = 'Mktcmpkey',right_on = 'cmpkey',suffixes = ('','_keys'))[cols]
dups = pd.concat(g for _, g in check.groupby(['contract_id']) if len(g) > 1)
dups = dups[(dups['written_date'] >= dups.startdate) & (dups['written_date'] <= dups.enddate)][['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'zip_code', 'state', 'BPO_flag', 'Mktcmpkey', 'MktChannel', 'MktsubChannel',       'MktCampaign', 'MktsubCampaign', 'obleadflag', 'ecommleadflag',
       'salesorc', 'MktChanneldigital', 'MktChannelBroad', 'MktChannelDM',
       'salesfunnel', 'subsalesfunnel', 'year', 'month', 'day', 'week','Mktpartner','channelmapping(marketingperformancereport)',
       'sub-channelmapping(marketingperformancereport)','startdate','enddate']].drop_duplicates()

check = check[~check.contract_id.isin(dups.contract_id)][['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'zip_code', 'state', 'BPO_flag', 'Mktcmpkey', 'MktChannel', 'MktsubChannel',       'MktCampaign', 'MktsubCampaign', 'obleadflag', 'ecommleadflag',
       'salesorc', 'MktChanneldigital', 'MktChannelBroad', 'MktChannelDM',
       'salesfunnel', 'subsalesfunnel', 'year', 'month', 'day', 'week','Mktpartner','channelmapping(marketingperformancereport)',
       'sub-channelmapping(marketingperformancereport)','startdate','enddate']]


mafacts_sales = check.append(dups)[['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'zip_code', 'state', 'BPO_flag', 'Mktcmpkey', 'MktChannel', 'MktsubChannel',       'MktCampaign', 'MktsubCampaign', 'obleadflag', 'ecommleadflag',
       'salesorc', 'MktChanneldigital', 'MktChannelBroad', 'MktChannelDM',
       'salesfunnel', 'subsalesfunnel', 'year', 'month', 'day', 'week','Mktpartner','channelmapping(marketingperformancereport)',
       'sub-channelmapping(marketingperformancereport)']]
mafacts_sales.columns = ['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',
       'product_description', 'zip_code', 'state', 'BPO_flag', 'Mktcmpkey',
       'MktChannel', 'MktsubChannel', 'MktCampaign', 'MktsubCampaign',
       'obleadflag', 'ecommleadflag', 'salesorc', 'MktChanneldigital',
       'MktChannelBroad', 'MktChannelDM', 'salesfunnel', 'subsalesfunnel',
       'year', 'month', 'day', 'week', 'Mktpartner',
       'channel_mapping',
       'subchannel_mapping']

mafacts_sales['channel_mapping'] = mafacts_sales['channel_mapping'].fillna('Unknown')
mafacts_sales['subchannel_mapping'] = mafacts_sales['subchannel_mapping'].fillna('Unknown')




# =============================================================================
#  Writing files and Tables
# =============================================================================

DBcon.SnowFlakeDB('apathak','ELT_DEV','ELT_DEV_RL').query('''drop table "PARTY"."CONTACT"."MA_FACTS_REPORT"''')

import keyring
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
engine = create_engine(URL(
    account = 'fn20555.us-east-1',
    user = 'apathak',
    password = keyring.get_password('Snowflake', 'apathak'),
    database = 'PARTY',
    schema = 'CONTACT',
    warehouse = 'ELT_DEV',
    role='ELT_DEV_RL',
))
connection = engine.connect()

mafacts_sales[['written_date', 'contract_id', 'company_code', 'sales_channel',
       'contract_status', 'last_name', 'first_name', 'name',  'product_description', 'BPO_flag', 'zip_code', 'state', 'Mktcmpkey', 'MktChannel', 'MktsubChannel',
       'MktCampaign','MktsubCampaign','channel_mapping','subchannel_mapping','salesfunnel', 'subsalesfunnel', 'year', 'month', 'day', 'week']].drop_duplicates().reset_index(drop = True).to_sql('MA_FACTS_REPORT', con=connection, method ='multi', if_exists='append', index=False, chunksize=5000)

connection.close()
engine.dispose()

summary.to_csv('C:\\Users\\apathak\\Documents\\2020 Projects\\MA Facts\\Data\\Interim Output\\Summary_' + str(dt.date.today()) + '.csv',index = False)
finaldf.to_csv('C:\\Users\\apathak\\Documents\\2020 Projects\\MA Facts\\Data\\Interim Output\\MatchBack_' + str(dt.date.today()) + '.csv',index = False)

summary.to_csv('C:\\Users\\apathak\\Documents\\2020 Projects\\MA Facts\\Data\\Final Output\\Summary.csv',index = False)
finaldf.to_csv('C:\\Users\\apathak\\Documents\\2020 Projects\\MA Facts\\Data\\Final Output\\MatchBack.csv',index = False)

print('refresh dashboard')

print(dt.datetime.now())
summary.to_csv(r'Y:\Marketing\MA Facts - LastTouch MatchBack'+'/Summary.csv',index = False)
finaldf.to_csv(r'Y:\Marketing\MA Facts - LastTouch MatchBack'+'/MatchBack.csv',index = False)
print(dt.datetime.now())
